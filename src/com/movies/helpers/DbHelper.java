package com.movies.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

	private static DbHelper dbHelperInstance = null;
	private static final int DATABASE_VERSION = 1;

	private DbHelper(Context context) {
		super(context, DatabaseConstants.DATABASE_NAME.toString(), null,
				DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	public static DbHelper getDbInstance(Context c) {
		if (dbHelperInstance == null) {
			dbHelperInstance = new DbHelper(c);
		}
		return dbHelperInstance;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE "
				+ DatabaseConstants.DATABASE_TABLE_MOVIE.toString() + " ("
				+ DatabaseConstants.KEY_ROWID_MOVIE.toString()
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ DatabaseConstants.KEY_MOVIEID_MOVIE.toString() + " INTEGER NOT NULL, "	
				+ DatabaseConstants.KEY_MOVIE_NAME.toString() + " TEXT NOT NULL, "
				+ DatabaseConstants.KEY_MOVIE_YEAR.toString() + " TEXT NOT NULL, "
				+ DatabaseConstants.KEY_MOVIE_RUNTIME.toString() + " TEXT NOT NULL);");
			
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS"
				+ DatabaseConstants.DATABASE_TABLE_MOVIE.toString());
		onCreate(db);
	}
}