package com.movies.helpers;

public class Movie {
	private long id;
	private String title;
	private String year;
	private String runtime;

	public Movie() {
	}

	public Movie(long id, String title, String year, String runtime) {
		this.id = id;
		this.title = title;
		this.year = year;
		this.runtime = runtime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

}