package com.movies.helpers;

import java.util.ArrayList;
import java.util.HashMap;

public class ELCommon {

	public String API_URL = "https://yts.to/api/v2/list_movies.json?limit=50&minimum_rating=7";
	public String ID_Movie="";
	private static ELCommon mInstance = null;
	
	public HashMap<String, String> movieWatched;

	public ArrayList<HashMap<String, String>> Watched;

	public ArrayList<String> Watched_ID_Movies = new ArrayList<String>();
	public ArrayList<String> Wished_ID_Movies = new ArrayList<String>();
	
	public ArrayList<String> Watched_TITLE_Movies = new ArrayList<String>();
	public ArrayList<HashMap<String, String>> moviesListWatched;
	public ArrayList<HashMap<String, String>> moviesListWish;
	public ArrayList<Movie> listMovies = new ArrayList<Movie>();
	public ArrayList<Movie> listMoviesWish = new ArrayList<Movie>();
	
	public String ID_WatchedMovie = "s";
	public String TITLE_WatchedMovie = "c";
	public String Year_WatchedMovie = "s";
	public String IMAGE_WatchedMovie = "gg";
	public String RunTime_WatchedMovie = "df";
	
	public Integer dodadeno=0;
	public Integer MAX=0;
	
	public Boolean CheckedWatchedMovie=false;
	protected ELCommon() {
	}

	public static synchronized ELCommon getInstance() {
		if (null == mInstance) {
			mInstance = new ELCommon();
		}
		return mInstance;
	}

}
