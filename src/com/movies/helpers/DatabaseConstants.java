package com.movies.helpers;

public enum DatabaseConstants {
	DATABASE_NAME("dbMoviInfo"),
	DATABASE_TABLE_MOVIE("movie"),
	DATABASE_VERSION("1"),
	
	KEY_ROWID_MOVIE("_idMovie"),
	KEY_MOVIEID_MOVIE("idMovie"),
	KEY_MOVIE_NAME("movieTitle"),
	KEY_MOVIE_YEAR("movieYear"),
	KEY_MOVIE_RUNTIME("movieRuntime")
	
	;
	
	
	private String nameAsString;
	
	private DatabaseConstants(String nameAsString){
		this.nameAsString = nameAsString;
	}
	
	@Override
	public String toString() {
		
		return this.nameAsString;
	}
}