package com.movies.list;

import com.mkyong.android.R;
import com.movies.helpers.ELCommon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

public class FiltersActivity extends Activity {
	public Spinner spinner, sortMovies, ratingMovies;
	private Button btnGoCheckFilterResults;
	private CheckBox checkBox;
	private Boolean izbrano = false;
	private EditText search_text;
	private static String url = "https://yts.to/api/v2/list_movies.json?limit=50";
	public static String delce_filter = "", search_parametars = "";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filters);
		spinner = (Spinner) findViewById(R.id.planets_spinner);
		sortMovies = (Spinner) findViewById(R.id.sort_movies);
		ratingMovies = (Spinner) findViewById(R.id.rating_movies);
		search_text = (EditText) findViewById(R.id.txtNameMovie);

		// za zanrovite
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.planets_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				izbrano = true;
				Log.d("IZBRANO", (String) parent.getItemAtPosition(position));
				delce_filter = (String) parent.getItemAtPosition(position);
				if (!delce_filter.equals("ALL")) {
					url = "https://yts.to/api/v2/list_movies.json?genre="
							+ delce_filter;
					search_text.setText("");
				}
				Log.d("URL SEGA", url);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				delce_filter = "";
				url = "https://yts.to/api/v2/list_movies.json?limit=50&minimum_rating=7";
				Log.d("URL SEGA", url);
			}

		});
		// kraj za znarovite

		// za sortiranjata
		ArrayAdapter<CharSequence> adapter_sort = ArrayAdapter
				.createFromResource(this, R.array.sort_array,
						android.R.layout.simple_spinner_item);

		adapter_sort
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		sortMovies.setAdapter(adapter_sort);
		sortMovies.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				izbrano = true;
				delce_filter = (String) parent.getItemAtPosition(position);
				if (delce_filter.equals("Date Added"))
					delce_filter = "date_added";
				if (delce_filter.equals("Downloads"))
					delce_filter = "download_count";
				if (delce_filter.equals("Like Counts"))
					delce_filter = "like_count";

				search_text.setText("");
				url = "https://yts.to/api/v2/list_movies.json?sort_by="
						+ delce_filter;
				Log.d("URL SEGA", url);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				delce_filter = "";
				url = "https://yts.to/api/v2/list_movies.json?limit=50&minimum_rating=7";
				Log.d("URL SEGA", url);
			}

		});
		// kraj za sortiranja

		checkBox = (CheckBox) findViewById(R.id.checkbox_asc);
		if (checkBox.isChecked()) {
			izbrano = true;
			url = "https://yts.to/api/v2/list_movies.json?limit=50&order_by=asc";
			Log.d("URL check box", url);
			search_text.setText("");
			ELCommon.getInstance().API_URL = url;
		} else {
			izbrano = true;

			search_text.setText("");
			url = "https://yts.to/api/v2/list_movies.json?limit=50&order_by=desc";
			Log.d("URL check box", url);
		}

		// za rating
		ArrayAdapter<CharSequence> adapter_rating = ArrayAdapter
				.createFromResource(this, R.array.rating_array,
						android.R.layout.simple_spinner_item);

		adapter_rating
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		ratingMovies.setAdapter(adapter_rating);
		ratingMovies.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				izbrano = true;
				search_text.setText("");
				delce_filter = (String) parent.getItemAtPosition(position);
				url = "https://yts.to/api/v2/list_movies.json?minimum_rating="
						+ delce_filter;
				Log.d("URL SEGA", url);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				delce_filter = "";
				url = "https://yts.to/api/v2/list_movies.json?limit=50&minimum_rating=7";
				Log.d("URL SEGA", url);
			}

		});
		// kraj za rating

		// klik na kopce za filtriranje
		btnGoCheckFilterResults = (Button) findViewById(R.id.btnFilterResult);
		btnGoCheckFilterResults.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				if (!izbrano)
					url = "https://yts.to/api/v2/list_movies.json?limit=50&minimum_rating=7";
				else {
					ELCommon.getInstance().API_URL = url;
				}
				if (search_text.length() > 1) {
					search_parametars = search_text.getText().toString();
					url = "https://yts.to/api/v2/list_movies.json?query_term="
							+ search_parametars;
					ELCommon.getInstance().API_URL = url;
				}
				Log.d("URL SEGA", ELCommon.getInstance().API_URL);
				Intent intent = new Intent(getApplicationContext(),
						MoviesActivity.class);
				startActivity(intent);
			}
		});

	}

}