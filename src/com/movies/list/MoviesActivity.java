package com.movies.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mkyong.android.R;
import com.movies.helpers.ELCommon;
import com.movies.helpers.ServiceHandler;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;

public class MoviesActivity extends ListActivity {
	private ProgressDialog pDialog;

	// URL to get contacts JSON
	private static String url = "";

	// JSON Node names
	private static final String TAG_MOVIES = "movies";
	private static final String TAG_ID = "id";
	private static final String TAG_TITLE = "title";
	private static final String TAG_TITLE_L = "title_long";
	private static final String TAG_YEAR = "year";
	private static final String TAG_RATING = "rating";
	private static final String TAG_RUNTIME = "runtime";
	private static final String TAG_GENRES = "genres";
	private static final String TAG_LANGUAGE = "language";
	private static final String TAG_MPA_RATING = "mpa_rating";
	private static final String TAG_IMAGE = "medium_cover_image";
	private static final String TAG_B_IMAGE = "background_image";
	public String title, image_cover, year, id, langue, runtime, title_l,
			rating, zanr = "", mpa_rating = "", background_image;
	// contacts JSONArray
	JSONArray movies = null;
	JSONArray genres = null;
	public Integer br = 0;
	HashMap<String, String> movie;

	final Context cont = this;
	// Hashmap for ListView
	ArrayList<HashMap<String, String>> moviesList;
	private Button btnFilters;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		url = ELCommon.getInstance().API_URL;
		moviesList = new ArrayList<HashMap<String, String>>();
		// ELCommon.getInstance().dodadeno
		// =
		// PreferenceManager.getDefaultSharedPreferences(cont).getInt("dodaden_film_watched",
		// ELCommon.getInstance().dodadeno);
		//
		// ELCommon.getInstance().MAX
		// =
		// PreferenceManager.getDefaultSharedPreferences(cont).getInt("max_brojac",ELCommon.getInstance().MAX);
		//
		// //
		// ELCommon.getInstance().Watched=PreferenceManager.getDefaultSharedPreferences(cont).getLong("api_token",
		// ELCommon.getInstance().Watched);
		// Log.d("MAX", String.valueOf(ELCommon.getInstance().MAX));
		btnFilters = (Button) findViewById(R.id.btnDetailsFilter);
		btnFilters.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click
				Intent intent = new Intent(getApplicationContext(),
						FiltersActivity.class);
				startActivity(intent);
			}
		});
		// for (int k = 0; k < ELCommon.getInstance().Watched_ID_Movies.size();
		// k++) {
		// Log.d("ID Za dodadeni",
		// ELCommon.getInstance().Watched_ID_Movies.get(k));
		// }

		ListView lv = getListView();
		// Listview on item click listener
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				// Log.d("KCO",
				// String.valueOf(position)+"***************"+String.valueOf(id));
				// Starting single contact activity
				Intent in = new Intent(getApplicationContext(),
						SingleMovieActivity.class);
				// in.putExtra(TAG_ID, id);

				String isd1 = moviesList.get(position).get(TAG_ID);

				Log.d("KCO", isd1);
				ELCommon.getInstance().ID_Movie = isd1;
				in.putExtra(TAG_ID, moviesList.get(position).get(TAG_ID));
				in.putExtra(TAG_TITLE, moviesList.get(position).get(TAG_TITLE));
				in.putExtra(TAG_YEAR, moviesList.get(position).get(TAG_YEAR));
				in.putExtra(TAG_RUNTIME,
						moviesList.get(position).get(TAG_RUNTIME));
				in.putExtra(TAG_GENRES, moviesList.get(position)
						.get(TAG_GENRES));
				in.putExtra(TAG_LANGUAGE,
						moviesList.get(position).get(TAG_LANGUAGE));
				in.putExtra(TAG_MPA_RATING,
						moviesList.get(position).get(TAG_MPA_RATING));
				in.putExtra(TAG_IMAGE, moviesList.get(position).get(TAG_IMAGE));
				in.putExtra(TAG_B_IMAGE,
						moviesList.get(position).get(TAG_B_IMAGE));
				// Log.d("KCO", image_cover);
				startActivity(in);

			}
		});

		// Calling async task to get json
		new GetMoviesList().execute();
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class GetMoviesList extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(MoviesActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			ServiceHandler sh = new ServiceHandler();

			// Making a request to url and getting response
			String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

			Log.d("Response: ", "> " + jsonStr);

			if (jsonStr != null) {
				try {
					JSONObject jsonObj = new JSONObject(jsonStr);

					// Getting JSON Array node
					JSONObject siteFilmovi = jsonObj.getJSONObject("data");

					movies = siteFilmovi.getJSONArray(TAG_MOVIES);

					// looping through All Contacts
					for (int i = 0; i < movies.length(); i++) {
						JSONObject c = movies.getJSONObject(i);

						id = c.getString(TAG_ID);

						title = c.getString(TAG_TITLE);
						title_l = c.getString(TAG_TITLE_L);
						year = c.getString(TAG_YEAR);
						rating = c.getString(TAG_RATING);

						runtime = c.getString(TAG_RUNTIME);
						int casovi = Integer.parseInt(runtime) / 60;
						int minuti = Integer.parseInt(runtime) % 60;
						zanr = "";
						genres = c.getJSONArray(TAG_GENRES);
						for (int j = 0; j < genres.length(); j++) {
							String ad = genres.getString(j);
							if (j == genres.length() - 1)
								zanr += ad;
							else
								zanr += ad + ", ";
						}

						langue = c.getString(TAG_LANGUAGE);
						mpa_rating = c.getString(TAG_MPA_RATING);
						image_cover = c.getString(TAG_IMAGE);
						background_image = c.getString(TAG_B_IMAGE);

						// tmp hashmap for single movie

						movie = new HashMap<String, String>();
						
							movie.put(TAG_ID, id);
							movie.put(TAG_TITLE, title);
							movie.put(TAG_TITLE_L, title_l);
							movie.put(TAG_YEAR, year);

							movie.put(TAG_RATING, rating);
							movie.put(TAG_RUNTIME, String.valueOf(casovi)
									+ "hr : " + String.valueOf(minuti) + "min");

							movie.put(TAG_GENRES, zanr);
							movie.put(TAG_LANGUAGE, langue);
							movie.put(TAG_MPA_RATING, mpa_rating);
							movie.put(TAG_IMAGE, image_cover);
							movie.put(TAG_B_IMAGE, background_image);
							
//							ostave prazni polinja	 
//							for (int k = 0; k < ELCommon.getInstance().Watched_ID_Movies.size();k++){
//									 
//									 if( movie.get(TAG_ID).equals(ELCommon.getInstance().Watched_ID_Movies.get(k)))
//									 {
//
////										 Log.d("TESSSSSSST elementi", movie.get(TAG_TITLE));
//										 movie.remove(TAG_TITLE);
//										 movie.remove(TAG_TITLE_L);
//										 movie.remove(TAG_YEAR);
//										 movie.remove(TAG_RATING);
//										 movie.remove(TAG_RUNTIME);
//										 movie.remove(TAG_RATING);
//										 movie.remove(TAG_GENRES);
//										 movie.remove(TAG_LANGUAGE);
//										 movie.remove(TAG_MPA_RATING);
//										 movie.remove(TAG_IMAGE);
//										 movie.remove(TAG_B_IMAGE);
//									 }
//								
//								 }
							
							
							moviesList.add(movie);
						
						br = i;
					}
//					za watched list
					for (int k = 0; k < ELCommon.getInstance().Watched_ID_Movies.size();k++)
					{
						for (int m = 0; m < moviesList.size();m++)
						{
						
							if(moviesList.get(m).get(TAG_ID).equals(ELCommon.getInstance().Watched_ID_Movies.get(k)))
							{
								Log.d("TESSSSSSST elementi", moviesList.get(m).get(TAG_TITLE));
								moviesList.remove(m);
							}
						}
						
					}
//					za wish list
					for (int k = 0; k < ELCommon.getInstance().Wished_ID_Movies.size();k++)
					{
						for (int m = 0; m < moviesList.size();m++)
						{
						
							if(moviesList.get(m).get(TAG_ID).equals(ELCommon.getInstance().Wished_ID_Movies.get(k)))
							{
								String b = moviesList.get(m).get(TAG_TITLE_L);
								String a= "FAV";
								moviesList.get(m).put(TAG_TITLE_L, b + "          -         " + a);

							}
						}
						
					}
				
					Log.d("Broj na filmovi", String.valueOf(br));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();
			/**
			 * Updating parsed JSON data into ListView
			 * */
			
			ListAdapter adapter = new SimpleAdapter(MoviesActivity.this,
					moviesList, R.layout.list_item, new String[] { TAG_TITLE_L,
							TAG_LANGUAGE, TAG_RATING }, new int[] { R.id.name,
							R.id.email, R.id.mobile });

			setListAdapter(adapter);
		}

	}

}