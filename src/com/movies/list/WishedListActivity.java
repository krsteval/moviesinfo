package com.movies.list;

import java.util.ArrayList;
import java.util.HashMap;

import com.mkyong.android.R;
import com.movies.helpers.ELCommon;
import com.movies.helpers.Movie;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class WishedListActivity extends ListActivity {
	private ProgressDialog pDialog;
	HashMap<String, String> movie;
	// Hashmap for ListView
	ArrayList<HashMap<String, String>> moviesListWish;

	final Context cont = this;

	private static final String TAG_ID = "a";
	private static final String TAG_TITLE = "b";
	private static final String TAG_RUNTIME = "v";
	private static final String TAG_YEAR = "g";
	ArrayList<Movie> moviesForCurrentWatchetList= new ArrayList<Movie>();
	Movie m;
	public String title = "", id = "", year = "", runtime = "";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wished);
        moviesListWish = new ArrayList<HashMap<String, String>>();
        m=new Movie();
       
		new GetMoviesWished().execute();
        
        
    }
    private class GetMoviesWished extends AsyncTask<Void, Void, Void> {

    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(WishedListActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(true);
			pDialog.show();

		}
		@Override
		protected Void doInBackground(Void... params) {

			 for (int i = 0; i < ELCommon.getInstance().listMoviesWish.size(); i++) {
		        	m = ELCommon.getInstance().listMoviesWish.get(i);
		        	
		        		movie = new HashMap<String, String>();
		        		movie.put(TAG_ID,String.valueOf( m.getId()));		        		
		        		ELCommon.getInstance().Wished_ID_Movies.add(String.valueOf( m.getId()));
			        	
		        		Log.d("ID_FILM", String.valueOf( m.getId()));
		        		movie.put(TAG_TITLE, m.getTitle());
		        		movie.put(TAG_YEAR, m.getYear());
		    			movie.put(TAG_RUNTIME, m.getRuntime());
		    					    			
		    			moviesListWish.add(movie);
					}


			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();
			ELCommon.getInstance().moviesListWish=moviesListWish;
//			PreferenceManager.getDefaultSharedPreferences(cont).edit().putString("api_token", ELCommon.getInstance().moviesListWatched).commit();
			 ListAdapter adapter = new SimpleAdapter(WishedListActivity.this,						
					 ELCommon.getInstance().moviesListWish, R.layout.list_item_wish,
						new String[] 
								{
				 		TAG_TITLE,
		        		TAG_RUNTIME, 
		        		TAG_YEAR}, 
						new int[] 
								{ 
								R.id.titleWish,
								R.id.YearWish,
								R.id.txtRuntimeWish
		    						});

		            setListAdapter(adapter);

		}
    }
}