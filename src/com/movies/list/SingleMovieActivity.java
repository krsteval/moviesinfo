package com.movies.list;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mkyong.android.R;
import com.movies.helpers.ELCommon;
import com.movies.helpers.Movie;
import com.movies.helpers.ServiceHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SingleMovieActivity extends Activity {

	// JSON node keys
	private static String url = "https://yts.to/api/v2/movie_details.json?movie_id=";
	private static final String TAG_ID = "id";
	private static final String TAG_TITLE = "title";
	private static final String TAG_TEST="";
	private static final String TAG_DESCRIPTION = "description_intro";
	private static final String TAG_LIKES = "like_count";
	private static final String TAG_CRITICS_SCORE = "rt_critics_score";
	private static final String TAG_YEAR = "year";

	private static final String TAG_RUNTIME = "runtime";
	private static final String TAG_GENRES = "genres";
	private static final String TAG_LANGUAGE = "language";
	private static final String TAG_MPA_RATING = "mpa_rating";
	private static final String TAG_IMAGE = "medium_cover_image";
	private static final String TAG_B_IMAGE = "background_image";
	JSONArray movies = null;
	JSONArray genres = null;
	TextView txtDescr;
	TextView like, score_c;
	public boolean flag=true,flag1=true;
	public Integer br = 0;
	final Context cont = this;
	public String title = "", year = "", image = "";
	private CheckBox checkBoxWatched, checkBoxWishList;
	HashMap<String, String> movie;
	HashMap<String, String> movie_watchet;
	Button btnWatched;
	Button btnWish;
	// Hashmap for ListView
	ArrayList<HashMap<String, String>> moviesList;
	ArrayList<HashMap<String, String>> moviesList_watched;
	ImageView img;
	LinearLayout backgroundImage;
	Bitmap bitmap;
	ProgressDialog pDialog;
	public String full_description = "", id = "", likes = "", runtime = "",
			critics_score = "";
	Movie film=new Movie();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_movie);
		txtDescr = (TextView) findViewById(R.id.txtDescription);

		like = (TextView) findViewById(R.id.txtLikes);
		score_c = (TextView) findViewById(R.id.txtCritScore);
		// getting intent data
		Intent in = getIntent();

		// Get JSON values from previous intent

		id = in.getStringExtra(TAG_ID);
		url = "https://yts.to/api/v2/movie_details.json?movie_id=" + id;

		Log.d("URL ZA Detali", url);
		title = in.getStringExtra(TAG_TITLE);
		year = in.getStringExtra(TAG_YEAR);
		runtime = in.getStringExtra(TAG_RUNTIME);
		String genres = in.getStringExtra(TAG_GENRES);
		String langue = in.getStringExtra(TAG_LANGUAGE);
		String mpaRating = in.getStringExtra(TAG_MPA_RATING);
		image = in.getStringExtra(TAG_IMAGE);
		String background_image = in.getStringExtra(TAG_B_IMAGE);

		// Displaying all values on the screen
		TextView lblTitle = (TextView) findViewById(R.id.title_label);
		TextView lblYear = (TextView) findViewById(R.id.year_label);
		TextView lblRuntime = (TextView) findViewById(R.id.runtime_label);
		TextView lblGenres = (TextView) findViewById(R.id.genres_label);
		TextView lblLangue = (TextView) findViewById(R.id.langue_label);
		TextView lblMpaRating = (TextView) findViewById(R.id.mpaRater_label);
		img = (ImageView) findViewById(R.id.imgCover);
		backgroundImage = (LinearLayout) findViewById(R.id.layout_details);

		btnWatched = (Button) findViewById(R.id.btnWatch);
		btnWish= (Button) findViewById(R.id.btnWish);
		
		
		
		lblTitle.setText(title);
		lblYear.setText(year);
		lblRuntime.setText(runtime);
		lblGenres.setText(genres);
		lblLangue.setText(langue);
		lblMpaRating.setText(mpaRating);
		new LoadImage().execute(image);
		new LoadImageB().execute(background_image);
		new GetMoviesDetail().execute();
		
		flag=true;

		ELCommon.getInstance().CheckedWatchedMovie=false;
		btnWatched.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.d("Vlez checked", id + " **** " + title);
				ELCommon.getInstance().CheckedWatchedMovie=true;

	
//				ELCommon.getInstance().TITLE_WatchedMovie = title;
//				ELCommon.getInstance().Year_WatchedMovie = year;
//				ELCommon.getInstance().IMAGE_WatchedMovie = image;
//				ELCommon.getInstance().RunTime_WatchedMovie = runtime;

//				Intent in = new Intent(getApplicationContext(),
//						WatchedListActivity.class);
//				
//				in.putExtra(ELCommon.getInstance().ID_WatchedMovie, id);
//				in.putExtra(ELCommon.getInstance().TITLE_WatchedMovie, title);
//				in.putExtra(ELCommon.getInstance().Year_WatchedMovie, year);
//				in.putExtra(ELCommon.getInstance().RunTime_WatchedMovie, runtime);
//				
//				startActivity(in);
				film.setId(Long.parseLong(id));
				film.setTitle(title);
				film.setYear(year);
				film.setRuntime(runtime);
				
				ELCommon.getInstance().listMovies.add(film);
				if(flag)
				{
					btnWatched.setEnabled(false);
				}
				flag=false;
//				ELCommon.getInstance().dodadeno++;
//        		PreferenceManager.getDefaultSharedPreferences(cont).edit().putInt("dodaden_film_watched", ELCommon.getInstance().dodadeno);
			}
		});

		ELCommon.getInstance().CheckedWatchedMovie=false;
		//	ZA WISH LIST
		flag1=true;
		btnWish.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.d("Vlez checked", id + " **** " + title);
				ELCommon.getInstance().CheckedWatchedMovie=true;
				film.setId(Long.parseLong(id));
				film.setTitle(title);
				film.setYear(year);
				film.setRuntime(runtime);
				
				ELCommon.getInstance().listMoviesWish.add(film);
				if(flag1)
				{


					btnWish.setEnabled(false);
				}
				flag1=false;
			}		
		});
		
	}

	private class LoadImage extends AsyncTask<String, String, Bitmap> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SingleMovieActivity.this);
			pDialog.setMessage("Loading Image ....");
			// pDialog.show();

		}

		protected Bitmap doInBackground(String... args) {
			try {
				// Log.d("url slika vo pivik", args[0]);
				bitmap = BitmapFactory.decodeStream((InputStream) new URL(
						args[0]).getContent());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
		}

		protected void onPostExecute(Bitmap image) {

			if (image != null) {
				img.setImageBitmap(image);
				// pDialog.dismiss();

			} else {

				// pDialog.dismiss();
				Toast.makeText(SingleMovieActivity.this,
						"Image Does Not exist or Network Error",
						Toast.LENGTH_SHORT).show();

			}
		}
	}

	private class LoadImageB extends AsyncTask<String, String, Bitmap> {

		protected Bitmap doInBackground(String... args) {
			try {
				// Log.d("url slika vo pivik", args[0]);
				bitmap = BitmapFactory.decodeStream((InputStream) new URL(
						args[0]).getContent());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
		}

		protected void onPostExecute(Bitmap image) {

			if (image != null) {
				BitmapDrawable ob = new BitmapDrawable(getResources(), image);
				backgroundImage.setBackgroundDrawable(ob);
				Drawable background = backgroundImage.getBackground();
				background.setAlpha(65);
				pDialog.dismiss();

			} else {

				pDialog.dismiss();
				Toast.makeText(SingleMovieActivity.this,
						"Image Does Not exist or Network Error",
						Toast.LENGTH_SHORT).show();

			}
		}
	}

	private class GetMoviesDetail extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(SingleMovieActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			ServiceHandler sh = new ServiceHandler();

			String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

			Log.d("Response: ", "> " + jsonStr);

			if (jsonStr != null) {
				try {
					JSONObject jsonObj = new JSONObject(jsonStr);

					// Getting JSON Array node
					JSONObject siteFilmovi = jsonObj.getJSONObject("data");

					full_description = siteFilmovi.getString(TAG_DESCRIPTION);
					Log.d("DESCRIPTION", full_description);

					likes = siteFilmovi.getString(TAG_LIKES);

					critics_score = siteFilmovi.getString(TAG_CRITICS_SCORE);

					if (pDialog.isShowing())
						pDialog.dismiss();

				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();
			txtDescr.setText(full_description);
			like.setText(likes);
			score_c.setText(critics_score);
			Log.d("Full Description", full_description);

		}

	}
	
}
