package com.movies.list;

import com.mkyong.android.R;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class MainActivity extends TabActivity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Resources ressources = getResources(); 
		TabHost tabHost = getTabHost(); 
		
		// FilmList tab
		Intent intentMoviesList = new Intent().setClass(this, MoviesActivity.class);
		TabSpec moviesListTab = tabHost
			.newTabSpec("MoviesList")
			.setIndicator("", ressources.getDrawable(R.drawable.icon_movieslist_config))
			.setContent(intentMoviesList);

		// WatchedList tab
		Intent intentWatchedList = new Intent().setClass(this, WatchedListActivity.class);
		TabSpec tabSpecApple = tabHost
			.newTabSpec(" WatchedList")
			.setIndicator("", ressources.getDrawable(R.drawable.icon_watchedlist_config))
			.setContent(intentWatchedList);
		
		
		// WishedList tab
		Intent intentWishList = new Intent().setClass(this, WishedListActivity.class);
		TabSpec wishedListTab = tabHost
			.newTabSpec("WishedList")
			.setIndicator("", ressources.getDrawable(R.drawable.icon_wishlist_config))
			.setContent(intentWishList);
	
		// add all tabs 
		tabHost.addTab(moviesListTab);
		tabHost.addTab(tabSpecApple);
		tabHost.addTab(wishedListTab);
		
		//set Windows tab as default (zero based)
		tabHost.setCurrentTab(0);
	}

}