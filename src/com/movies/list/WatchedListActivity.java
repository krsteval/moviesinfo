package com.movies.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mkyong.android.R;
import com.movies.helpers.ELCommon;
import com.movies.helpers.Movie;





import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

public class WatchedListActivity extends ListActivity {
	private ProgressDialog pDialog;
	HashMap<String, String> movie;
	// Hashmap for ListView
	ArrayList<HashMap<String, String>> moviesList;

	final Context cont = this;

	private static final String TAG_ID = "a";
	private static final String TAG_TITLE = "b";
	private static final String TAG_RUNTIME = "v";
	private static final String TAG_YEAR = "g";
	ArrayList<Movie> moviesForCurrentWatchetList= new ArrayList<Movie>();
	Movie m;
	public String title = "", id = "", year = "", runtime = "";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_watched);
        moviesList = new ArrayList<HashMap<String, String>>();
        m=new Movie();
        String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2" };
        
//        Integer brojac=	ELCommon.getInstance().dodadeno
//    			= PreferenceManager.getDefaultSharedPreferences(cont).getInt("dodaden_film_watched", ELCommon.getInstance().dodadeno);
//		
//        if(brojac>ELCommon.getInstance().MAX)
//        {
//        	ELCommon.getInstance().MAX=brojac;
//        	Log.d("MAX", String.valueOf(brojac));
//        	
//
//    		PreferenceManager.getDefaultSharedPreferences(cont).edit().putInt("max_brojac",ELCommon.getInstance().MAX);
//        }
            // use your custom layout
//	        Intent in = getIntent();
//	
//	        id = in.getStringExtra(ELCommon.getInstance().ID_WatchedMovie);
//	        title = in.getStringExtra(ELCommon.getInstance().TITLE_WatchedMovie);
//			year = in.getStringExtra(ELCommon.getInstance().Year_WatchedMovie);
//			runtime = in.getStringExtra(ELCommon.getInstance().RunTime_WatchedMovie);
//			 
//			movie = new HashMap<String, String>();
//	
//			movie.put(TAG_TITLE, title);
//			movie.put(TAG_YEAR, year);
//			movie.put(TAG_RUNTIME, runtime);
//			
//			
//			moviesList.add(movie);
//	
//			 Log.d("VO WATCHED godina", year  +"," + title); 
//	            ListAdapter adapter = new SimpleAdapter(WatchedListActivity.this,
//	    				
//	    				moviesList, R.layout.list_item_watched,
//	    				new String[] 
//	    						{
//	            		TAG_TITLE,
//	            		TAG_RUNTIME, 
//	            		TAG_YEAR}, 
//	    				new int[] 
//	    						{ 
//	    						R.id.titleWatched,
//	    						R.id.YearWatched,
//	    						R.id.txtRuntimeWatched
//	        						});
//	    
//	                setListAdapter(adapter);
//        } 
		new GetMoviesWatched().execute();
        
        
    }
    private class GetMoviesWatched extends AsyncTask<Void, Void, Void> {

    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(WatchedListActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(true);
			pDialog.show();

		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			 for (int i = 0; i < ELCommon.getInstance().listMovies.size(); i++) {
		        	m = ELCommon.getInstance().listMovies.get(i);
		        	Log.d("AAAAAAAAAAAAAAAAAa", m.getTitle() );
		        	movie = new HashMap<String, String>();
		        	

//		        		PreferenceManager.getDefaultSharedPreferences(cont).edit().putString("title_watched", m.getTitle());
//		        		ELCommon.getInstance().TITLE_WatchedMovie=PreferenceManager.getDefaultSharedPreferences(cont).getString("title_watched", m.getTitle());
		        		movie.put(TAG_ID,String.valueOf( m.getId()));
		        		
		        		ELCommon.getInstance().Watched_ID_Movies.add(String.valueOf( m.getId()));
			        	
		        		Log.d("ID_FILM", String.valueOf( m.getId()));
		        		movie.put(TAG_TITLE, m.getTitle());
		        		ELCommon.getInstance().Watched_TITLE_Movies.add(m.getTitle());
//		    			Log.d("TEST",  PreferenceManager.getDefaultSharedPreferences(cont).getString("title_watched", movie.get(TAG_TITLE)));
		    			movie.put(TAG_YEAR, m.getYear());

		    			movie.put(TAG_RUNTIME, m.getRuntime());
		    			
		    			
		    			moviesList.add(movie);
		        	
				}


			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();
			ELCommon.getInstance().moviesListWatched=moviesList;
//			PreferenceManager.getDefaultSharedPreferences(cont).edit().putString("api_token", ELCommon.getInstance().moviesListWatched).commit();
			 ListAdapter adapter = new SimpleAdapter(WatchedListActivity.this,						
					 ELCommon.getInstance().moviesListWatched, R.layout.list_item_watched,
						new String[] 
								{
				 		TAG_TITLE,
		        		TAG_RUNTIME, 
		        		TAG_YEAR}, 
						new int[] 
								{ 
								R.id.titleWatched,
								R.id.YearWatched,
								R.id.txtRuntimeWatched
		    						});

		            setListAdapter(adapter);

		}
    
    
    }
}